﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web_Programlama_2.Ödev.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Ram()
        {
            ViewBag.Message = "Çeşitli markalarda,birçok özellikte ramler...";
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Hakkımızda bilmek istediğiniz her şey...";
            return View();
        }

        public ActionResult Card()
        {
            ViewBag.Message = "Çeşitli markalarda,birçok özellikte Ekran Kartları...";
            return View();
        }

        public ActionResult Other()
        {
            ViewBag.Message = "Çeşitli markalarda donanım parçaları,aksesuarlar vs...";
            return View();
        }

        public ActionResult Processor()
        {
            ViewBag.Message = "Çeşitli markalarda,birçok özellikte İşlemciler...";
            return View();
        }

    }
}